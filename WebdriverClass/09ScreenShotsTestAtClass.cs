﻿using System;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System.Collections.ObjectModel;
using System.IO;

namespace WebdriverClass
{
    class ScreenShotsTestAtClass : TestBase
    {
        [Test]
        public void ScreenShots()
        {
            Driver.Navigate().GoToUrl("http://www.elvira.hu");

            string baseDirectory = Path.GetFullPath(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "..\\..\\..\\screenshot\\"));
            //Maximize browser window
            Driver.Manage().Window.Maximize();
            //Create a screenshot and save the file as screenshot.png
            var screenshot = ((ITakesScreenshot)Driver).GetScreenshot();
            screenshot.SaveAsFile(baseDirectory + "screenshot.png", ScreenshotImageFormat.Png);
            Assert.IsTrue(File.Exists(baseDirectory + "screenshot.png"));
        }
    }
}
