﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using NUnit.Framework;
using WebdriverClass.Beadando;

namespace WebdriverClass
{
    class BeadandoPageObjectTest : TestBase
    {
        [Test, TestCaseSource("TestData")]
        public void SearchTest(string name)
        {
            SourcePageWidget sourcePageWidget = SourcePage.Navigate(Driver).GetSourcePageWidget();
            LandingPage landingPage = sourcePageWidget.SearchFor(name);
            
            //Only this type and amount of waiting gives consistent acceptance
            Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);
            LandingPageWidget landingPagelWidget = landingPage.GetLandingPageWidget();
            Assert.That(landingPagelWidget?.GetNoOfResults(), Is.GreaterThan(0));

        }

        static IEnumerable TestData()
        {
            var doc = XElement.Load(System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory) + "\\brands.xml");
            return
                from vars in doc.Descendants("testData")
                let name = vars.Attribute("name").Value
                select new object[] { name };
        }
    }
}
