﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebdriverClass.PagesAtClass;

namespace WebdriverClass.Beadando
{
    class SourcePage : BasePage
    {
       public SourcePage(IWebDriver webDriver) : base(webDriver)
        {
        }

        public static SourcePage Navigate(IWebDriver webDriver)
        {            
            webDriver.Url = "https://totalcar.hu/";
            return new SourcePage(webDriver);
        }
        public SourcePageWidget GetSourcePageWidget()
        {
            return new SourcePageWidget(Driver);
        }

        public LandingPageWidget GetLandingPageWidget()
        {
            return new LandingPageWidget(Driver);
        }
    }
}
