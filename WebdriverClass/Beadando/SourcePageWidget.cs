﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebdriverClass.PagesAtClass;

namespace WebdriverClass.Beadando
{
    class SourcePageWidget : BasePage
    {
        public SourcePageWidget(IWebDriver driver) : base(driver)
        {
        }

        public IWebElement SearchTextBox => Driver.FindElement(By.ClassName("field-24h-search"));
        public IWebElement InitialSearchButton => Driver.FindElement(By.CssSelector("a[class='btn open-search-form-btn search']"));
        public IWebElement NewSearchButton => Driver.FindElement(By.CssSelector("button[class='search btn-24h']"));
        public IWebElement AcceptCookieButton => Driver.FindElement(By.CssSelector("button[class=' css-k8o10q']"));

        public void SetSearchText(string text)
        {
            SearchTextBox.SendKeys(text);
        }
        public LandingPage SearchFor(string text)
        {
            AcceptCookieButton.Click();
            InitialSearchButton.Click();
            SetSearchText(text);
            return ClickSearchButton();
        }

        public LandingPage ClickSearchButton()
        {
            NewSearchButton.Click();
            return new LandingPage(Driver);
        }
    }
}
