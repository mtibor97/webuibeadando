﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebdriverClass.PagesAtClass;

namespace WebdriverClass.Beadando
{
    class LandingPageWidget : BasePage
    {
        public LandingPageWidget(IWebDriver driver) : base(driver)
        {
        }

        private IWebElement articleList => Driver.FindElement(By.Id("talalatok"));

        private List<IWebElement> Articles => articleList.FindElements(By.XPath(".//li[@class]")).ToList();

        public int GetNoOfResults()
        {
            return Articles.Count;
        }
    }
}
