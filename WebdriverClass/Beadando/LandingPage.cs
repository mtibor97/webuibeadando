﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebdriverClass.PagesAtClass;

namespace WebdriverClass.Beadando
{
    class LandingPage : BasePage
    {
        public LandingPage(IWebDriver webDriver) : base(webDriver)
        {
        }
        public LandingPageWidget GetLandingPageWidget()
        {
            return new LandingPageWidget(Driver);
        }
    }
}
